<?php

namespace App\Controller;

use App\Entity\Cabinet;
use App\Entity\Location;
use App\Entity\Reservation;
use App\Entity\Schedule;
use App\Entity\User;
use App\Entity\UserHoliday;
use App\Form\Type\ReservationType;
use App\Repository\UserRepository;
use App\Service\LocationService;
use App\Service\NotificationService;
use App\Service\ReservationService;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;
use Doctrine\ORM\EntityManagerInterface;


class DefaultController extends AbstractController
{
    /**
     * @Route("/test", name="index")
     */
    public function index(Request $request, EntityManagerInterface $entityManager)
    {
        return  new Response('Works');
    }



}