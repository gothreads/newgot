<?php
namespace App\Admin;


//use App\Entity\Holiday;
use App\Entity\User;
//use App\Entity\UserHoliday;
//use App\Service\ReservationService;
//use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DateTimePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use FOS\UserBundle\Doctrine\UserManager as ObjectManager;
use Doctrine\ORM\EntityManager;


class UserAdmin  extends AbstractAdmin
{

    /** @var ObjectManager */
    private $objectManager;

    /** KernelInterface $appKernel */
    private $appKernel;

    /** @var EntityManager */
    private $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ObjectManager $objectManager
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param mixed $appKernel
     */
    public function setAppKernel($appKernel): void
    {
        $this->appKernel = $appKernel;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username', null, ['show_filter'=>true])
            ->add('email', null, ['show_filter'=>true])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('username')
            ->add('email')
            ->add('imageName', null, ['template' => 'admin/user/list_image.html.twig'])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var User $object */
        $object = $this->getSubject();

        $formMapper
            ->tab('General')
                ->add('username')
                ->add('name')
                ->add('email')
                ->add('plainPassword', TextType::class, ['label' => 'Password', 'required' => false])
                ->add('roles', ChoiceType::class, [
                    'mapped' => false,
                    'multiple' => true,
                    'choices' => ['ROLE_ADMIN' => 'ROLE_ADMIN', 'ROLE_CLIENT' => 'ROLE_CLIENT', 'ROLE_PRESTATOR' => 'ROLE_PRESTATOR'],
                    'attr' => ['class' => 'form-rolles'],
                    'data' => is_object($object) ? $object->getRoles() : []
                ])
                ->add('enabled')
            ->end()
        ;

    }
}