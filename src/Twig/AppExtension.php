<?php
namespace App\Twig;

use App\Entity\Reservation;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('cancellink', [$this, 'encodeReservationDetails']),
        ];
    }

    public function encodeReservationDetails(Reservation $reservation)
    {
        return base64_encode(serialize(['id' => $reservation->getId(), 'email' => $reservation->getClientEmail()]));
    }
}