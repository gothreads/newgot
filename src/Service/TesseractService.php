<?php


namespace App\Service;

use thiagoalessio\TesseractOCR\Command;
use thiagoalessio\TesseractOCR\TesseractOCR;


class TesseractService
{
    const SERVICE_NAME = 'tesseract_service';

    public function getText($path)
    {
        $command = new Command();
        $command->options[]="-l ron";
        $instance = new TesseractOCR($path);
        $text = $instance->run();
        setlocale(LC_CTYPE, 'ro_RO');
        return iconv('UTF-8', 'ASCII//TRANSLIT',$text);
    }
}