<?php


namespace App\Service\ServiceTrait;


use App\Service\IngredientsService;

trait IngredientsTrait
{
    /**
     * @var IngredientsService
     */
    private $ingredients;

    /**
     * @return IngredientsService
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @param IngredientsService $ingredients
     * @return IngredientsTrait
     */
    public function setIngredients($ingredients)
    {
        $this->ingredients = $ingredients;
        return $this;
    }
}