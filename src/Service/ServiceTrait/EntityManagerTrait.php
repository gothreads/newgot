<?php

namespace App\Service\ServiceTrait;


use Doctrine\ORM\EntityManager;

trait EntityManagerTrait
{

    /**
     * @var EntityManagera
     */
    private $entityManager;

    /**
     * @return EntityManagera
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManagera $entityManager
     * @return EntityManagerTrait
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }
}