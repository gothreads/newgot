<?php


namespace App\Service\ServiceTrait;

use App\Service\TesseractService;

trait TesseractTrait
{
    /**
     * @var TesseractService
     */
    private $ocrEngine;

    /**
     * @return TesseractService
     */
    public function getOcrEngine()
    {
        return $this->ocrEngine;
    }

    /**
     * @param TesseractService $ocrEngine
     * @return TesseractTrait
     */
    public function setOcrEngine($ocrEngine)
    {
        $this->ocrEngine = $ocrEngine;
        return $this;
    }
}