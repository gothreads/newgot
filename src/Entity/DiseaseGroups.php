<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DiseaseGroups
 *
 * @ORM\Table(name="disease_groups")
 * @ORM\Entity
 */
class DiseaseGroups
{
    use EntityTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var \App\Entity\Diseases[]
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Diseases", mappedBy="diseases")
     */
    private $diseases;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
        $this->diseases = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return DiseaseGroups
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Diseases[]
     */
    public function getDiseases()
    {
        return $this->diseases;
    }

    /**
     * @param Diseases[] $diseases
     * @return DiseaseGroups
     */
    public function setDiseases($diseases)
    {
        $this->diseases = $diseases;
        return $this;
    }

    public function __toString()
    {
     return $this->getName();
    }

}
