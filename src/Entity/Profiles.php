<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Profiles
 *
 * @ORM\Table(name="profiles", indexes={@ORM\Index(name="IDX_8B308530A76ED395", columns={"user_id"})})
 * @ORM\Entity
 */
class Profiles
{
   use EntityTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname", type="string", length=100, nullable=true)
     */
    private $surname;

    /**
     * @var int|null
     *
     * @ORM\Column(name="age", type="integer", nullable=true)
     */
    private $age;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sex", type="string", length=100, nullable=true)
     */
    private $sex;

    /**
     * @var int|null
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @var int|null
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     */
    private $height;

    /**
     * @var FosUser
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\FosUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var Diseases[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Diseases", inversedBy="profile")
     * @ORM\JoinTable(name="profiles_diseases",
     *   joinColumns={
     *     @ORM\JoinColumn(name="profile_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="disease_id", referencedColumnName="id")
     *   }
     * )
     */
    private $diseases;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
        $this->diseases = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Profiles
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param null|string $surname
     * @return Profiles
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int|null $age
     * @return Profiles
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param null|string $sex
     * @return Profiles
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     * @return Profiles
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     * @return Profiles
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return FosUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param FosUser $user
     * @return Profiles
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Diseases[]
     */
    public function getDiseases()
    {
        return $this->diseases;
    }

    /**
     * @param Diseases[] $diseases
     * @return Profiles
     */
    public function setDiseases($diseases)
    {
        $this->diseases = $diseases;
        return $this;
    }



}
