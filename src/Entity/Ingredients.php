<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ingredients
 *
 * @ORM\Table(name="ingredients", indexes={@ORM\Index(name="IDX_4B60114F727ACA70", columns={"parent_id"})})
 * @ORM\Entity
 */
class Ingredients
{
    use EntityTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nocivity", type="string", length=100, nullable=true)
     */
    private $nocivity;

    /**
     * @var float|null
     *
     * @ORM\Column(name="weight", type="float", precision=10, scale=0, nullable=true)
     */
    private $weight;

    /**
     * @var float|null
     *
     * @ORM\Column(name="product_weight", type="float", precision=10, scale=0, nullable=true)
     */
    private $productWeight;

    /**
     * @var Ingredients
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Ingredients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Efects", mappedBy="ingredient")
     */
    private $efect;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
        $this->efect = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param null|string $code
     * @return Ingredients
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Ingredients
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNocivity()
    {
        return $this->nocivity;
    }

    /**
     * @param null|string $nocivity
     * @return Ingredients
     */
    public function setNocivity($nocivity)
    {
        $this->nocivity = $nocivity;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     * @return Ingredients
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getProductWeight()
    {
        return $this->productWeight;
    }

    /**
     * @param float|null $productWeight
     * @return Ingredients
     */
    public function setProductWeight($productWeight)
    {
        $this->productWeight = $productWeight;
        return $this;
    }

    /**
     * @return Ingredients
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Ingredients $parent
     * @return Ingredients
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEfect()
    {
        return $this->efect;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $efect
     * @return Ingredients
     */
    public function setEfect($efect)
    {
        $this->efect = $efect;
        return $this;
    }

}
