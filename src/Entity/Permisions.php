<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permisions
 *
 * @ORM\Table(name="permisions", indexes={@ORM\Index(name="FK_permisions_diseases", columns={"disease_id"}), @ORM\Index(name="FK_permisions_ingredients", columns={"ingredient_id"})})
 * @ORM\Entity
 */
class Permisions
{
   use EntityTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="permis", type="string", length=30, nullable=false)
     */
    private $permis;

    /**
     * @var Ingredients
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Ingredients")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ingredient_id", referencedColumnName="id")
     * })
     */
    private $ingredient;

    /**
     * @var Diseases
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Diseases")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disease_id", referencedColumnName="id")
     * })
     */
    private $disease;

    /**
     * Permisions constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }


    /**
     * @return string
     */
    public function getPermis()
    {
        return $this->permis;
    }

    /**
     * @param string $permis
     * @return Permisions
     */
    public function setPermis($permis)
    {
        $this->permis = $permis;
        return $this;
    }

    /**
     * @return Ingredients
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param Ingredients $ingredient
     * @return Permisions
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
        return $this;
    }

    /**
     * @return Diseases
     */
    public function getDisease()
    {
        return $this->disease;
    }

    /**
     * @param Diseases $disease
     * @return Permisions
     */
    public function setDisease($disease)
    {
        $this->disease = $disease;
        return $this;
    }


}
