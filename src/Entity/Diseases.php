<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Diseases
 *
 * @ORM\Table(name="diseases", indexes={@ORM\Index(name="IDX_F762064765317422", columns={"disease_group_id"}), @ORM\Index(name="IDX_F7620647C4235953", columns={"disease_type_id"})})
 * @ORM\Entity
 */
class Diseases
{
    use EntityTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var DiseaseGroups
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\DiseaseGroups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disease_group_id", referencedColumnName="id")
     * })
     */
    private $diseaseGroup;

    /**
     * @var DiseaseTypes
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\DiseaseTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disease_type_id", referencedColumnName="id")
     * })
     */
    private $diseaseType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Profiles", mappedBy="disease")
     */
    private $profile;

    /**
     * Constructor
     */
    public function __construct()
    {
//        $this->createdAt = new \DateTime('now');
//        $this->updatedAt = new \DateTime('now');
        $this->profile = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return Diseases
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return DiseaseGroups
     */
    public function getDiseaseGroup()
    {
        return $this->diseaseGroup;
    }

    /**
     * @param DiseaseGroups $diseaseGroup
     * @return Diseases
     */
    public function setDiseaseGroup($diseaseGroup)
    {
        $this->diseaseGroup = $diseaseGroup;
        return $this;
    }

    /**
     * @return DiseaseTypes
     */
    public function getDiseaseType()
    {
        return $this->diseaseType;
    }

    /**
     * @param DiseaseTypes $diseaseType
     * @return Diseases
     */
    public function setDiseaseType($diseaseType)
    {
        $this->diseaseType = $diseaseType;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $profile
     * @return Diseases
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }

    public function __toString()
    {
     return $this->getName();
    }

}
