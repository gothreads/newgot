<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DiseaseTypes
 *
 * @ORM\Table(name="disease_types")
 * @ORM\Entity
 */
class DiseaseTypes
{
    use EntityTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * DiseaseTypes constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }


    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     * @return DiseaseTypes
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function __toString()
    {
     return $this->getName();
    }


}
