<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Efects
 *
 * @ORM\Table(name="efects")
 * @ORM\Entity
 */
class Efects
{
    use EntityTrait;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Ingredients", inversedBy="efect")
     * @ORM\JoinTable(name="ingredients_efects",
     *   joinColumns={
     *     @ORM\JoinColumn(name="efect_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ingredient_id", referencedColumnName="id")
     *   }
     * )
     */
    private $ingredient;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
        $this->ingredient = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     * @return Efects
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIngredient()
    {
        return $this->ingredient;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $ingredient
     * @return Efects
     */
    public function setIngredient($ingredient)
    {
        $this->ingredient = $ingredient;
        return $this;
    }

}
